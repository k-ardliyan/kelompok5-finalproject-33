<div class="l-navbar" id="nav-bar">
    <nav class="nav">
        <div>
            <a href="{{route('welcome')}}" class="nav_logo">
                <i class='bx bx-layer nav_logo-icon'></i>
                <span class="nav_logo-name">FORUM DISKUSI</span>
            </a>
            <div class="nav_list">
                <a href="{{route('dashboard')}}" class="nav_link {{Route::is('dashboard') ? 'active' : ''}}">
                    <i class='bx bx-grid-alt nav_icon'></i>
                    <span class="nav_name">Dashboard</span>
                </a>
                <a href="{{route('create-question')}}" class="nav_link {{Route::is('create-question') ? 'active' : ''}}">
                    <i class='bx bx-message-square-detail nav_icon'></i>
                    <span class="nav_name">Buat Pertanyaan</span>
                </a>
            </div>
        </div>
        <div>
            <a href="/profile/{{Auth::id()}}" class="nav_link {{Route::is('profile') ? 'active' : ''}}">
                <i class='bx bx-user nav_icon'></i>
                <span class="nav_name">My Profile</span>
            </a>
            <a href="{{route('logout')}}" class="nav_link">
                <i class='bx bx-log-out nav_icon'></i>
                <span class="nav_name">Logout</span>
            </a>
        </div>
    </nav>
</div>
