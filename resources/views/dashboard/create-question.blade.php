@extends('layouts.master')

@push('styles')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.css" rel="stylesheet">
    <style>
        .bootstrap-tagsinput .tag {
            margin-right: 2px;
            color: #ffffff;
            background: #2196f3;
            padding: 3px 7px;
            border-radius: 3px;
        }
        .bootstrap-tagsinput {
            width: 100%;
        }
    </style>
@endpush

@section('content')
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">Buat Pertanyaan</h5>
            <hr>
            <form action="/question" method="POST" enctype="multipart/form-data">
              @csrf
                <div class="mb-3">
                    <label for="title" class="form-label">Judul Pertanyaan</label>
                    <input name='title' class="form-control" id="title" placeholder="Masukkan judul pertanyaan" required>
                </div>
                @error('title')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="mb-3">
                  <label for="content" class="form-label">Detail Pertanyaan</label>
                  <textarea class="form-control" name='content' id="content" placeholder="Masukkan detail pertanyaan"></textarea>
                </div>
                @error('content')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="mb-3">
                    <label for="tags" class="form-label">Hastag</label>
                    <input type="text" data-role="tagsinput" name='tags' class="form-control" id="tags" placeholder="Masukkan hastag" required>
                </div>
                @error('tag')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="mb-3">
                  <label for="image" class="form-label">Upload Gambar <span style="font-size: .8rem">*(opsional)</span></label>
                  <input name="image" id="image" type="file" class="form-control" aria-label="file example" required>
                </div>
                @error('image')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                {{-- <input name="user_id" type="hidden" value="1"> --}}
                <div class="mb-3">
                  <button class="btn btn-primary" type="submit"><i class='bx bx-send'></i> Kirim Pertanyaan</button>
                </div>
              </form>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.js"></script>
    <script src="https://cdn.tiny.cloud/1/mx0hg6m4ui31cy1igzvxgbkvg2ui5p76rj0b5uen9754y0xa/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>
    <script>
        tinymce.init({
          selector: 'textarea',
          plugins: 'a11ychecker advcode casechange export formatpainter image editimage linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tableofcontents tinycomments tinymcespellchecker',
          toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter image editimage pageembed permanentpen table tableofcontents',
          toolbar_mode: 'floating',
          tinycomments_mode: 'embedded',
          tinycomments_author: 'Author name',
        });
      </script>
@endpush
