@extends('layouts.master')

@section('content')
<div class="card">
    <div class="card-body">
        <div class="question">
            <div class="d-flex mb-3">
                <div class="me-3">
                    <img src="{{asset('images/'.$questions->user->profile->avatar)}}" alt="" class="rounded-circle" width="50">
                </div>
                <div>
                    <h5 class="mb-0">
                        <a href="#" class="text-dark">
                            <span>{{$questions->user->profile->fullname}}</span>
                        </a>
                    </h5>
                    <p class="text-muted mb-0">
                        <small>
                            <i class="bx bx-time"></i>
                            @php
                                $authored_time = $questions->created_at;
                                $authored_status = "Created ";

                                if ($questions->created_at != $questions->updated_at) {
                                    $authored_time = $questions->updated_at;
                                    $authored_status = "Updated ";
                                }
                            @endphp
                            <span class="ms-1">{{$authored_status.$authored_time->diffForHumans()}}</span>
                        </small>
                    </p>
                </div>
            </div>
            <h5><strong>{{$questions->title}}</strong></h5>
            <p>{!!$questions->content!!}</p>
            <img src="{{asset('images/'.$questions->image)}}" alt="" style="max-width: 100%">
            <div class="d-flex justify-content-between">
                <div class="align-self-center">
                    @forelse ($questions->tags as $tag)
                        <span class="badge bg-info">{{ $tag->name }}</span>
                    @empty
                        <span class="badge bg-info"></span>
                    @endforelse
                </div>
                <div class="align-self-center">
                    <form action="/question/{{$questions->id}}" method="POST">
                        @csrf
                        @method('delete')
                        <a href="#" class="btn btn-outline-success mx-1"><i class='bx bx-share-alt' ></i> Bagikan</a>
                        @if($current_user->id == $questions->user->id)
                            <a href="/question/{{$questions->id}}/edit" class="btn btn-outline-info"><i class='bx bx-plus' ></i> Edit</a>
                            <input type="submit" class="btn btn-outline-danger" value="Delete">@endif
                    </form>
                </div>
            </div>
        </div>

        <hr class="my-4">
        <h5 class="mb-3"><strong>Jawaban</strong></h5>

        <div class="answer-list">
            <div class="mb-3">
                @forelse ($questions->answer as $answer)
                    <div class="d-flex mb-3">
                        <div class="me-3">
                            <img src="{{asset('images/'.$answer->user->profile->avatar)}}" alt="" class="rounded-circle" width="50">
                        </div>
                        <div>
                            <h5 class="mb-0">
                                <a href="#" class="text-dark">
                                    <span>{{$answer->user->profile->fullname}}</span>
                                </a>
                            </h5>
                            <p class="text-muted mb-0">
                                <small>
                                    <i class="bx bx-time"></i>
                                    @php
                                            $authored_time = $answer->created_at;
                                            $authored_status = "Created ";

                                            if ($answer->created_at != $answer->updated_at) {
                                                $authored_time = $answer->updated_at;
                                                $authored_status = "Updated ";
                                            }
                                        @endphp
                                        <span class="ms-1">{{$authored_status.$authored_time->diffForHumans()}}</span>
                                </small>
                            </p>
                        </div>
                    </div>
                    <div class="card-body">
                    {{-- <pre class="bg-light p-4"> --}}
                        {!!$answer->content!!}
                    {{-- </pre> --}}
                    </div>
                    <div class="text-end">
                        @if($current_user->id == $answer->user->id)
                            <form action="/answer/{{$answer->id}}" method="POST">
                                @csrf
                                @method('delete')
                                    <a href="/answer/{{$questions->id}}/{{$answer->id}}/edit" class="btn btn-outline-info"><i class='bx bx-plus' ></i> Edit</a>
                                <input type="submit" class="btn btn-outline-danger" value="Delete">
                            </form>
                        @endif
                    </div>
                @empty
                <div class="d-flex mb-3">

                </div>
                <p class="bg-light p-4">Belum ada jawaban</p>
                <div class="text-end">
                </div>
                @endforelse
            </div>
        </div>

        <div class="answer">
            <div class="d-flex mb-3">
                <div class="me-3">
                    <img src="{{asset('images/'.$current_user->avatar)}}" alt="" class="rounded-circle" width="50">
                </div>
                <div>
                    <h5 class="mb-0">
                        <a href="#" class="text-dark">
                            <strong>{{$current_user->fullname}}</strong>
                        </a>
                    </h5>
                    {{-- <p class="text-muted mb-0"> --}}
                        {{-- <small> --}}
                            {{-- <i class="bx bx-time"></i> --}}
                            {{-- <span class="ms-1">13 jam yang lalu</span> --}}
                        {{-- </small> --}}
                    {{-- </p> --}}
                </div>
            </div>
            <form action="/answer" method="POST">
                @csrf
                <div class="form-floating mb-3">
                    <textarea class="form-control" name="answer" id="answer" placeholder="Leave a comment here" style="height: 100px"></textarea>
                    <label for="answer">Tuliskan jawabanmu disini ya</label>
                </div>
                @error('answer')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <input type="hidden" name="question_id" id="question_id" value="{{$questions->id}}" />
                <button type="submit" class="btn btn-primary float-end"><i class='bx bx-send' ></i> Kirim Jawaban</button>
            </form>
        </div>
    </div>
</div>

@endsection

@push('scripts')
<script src="https://cdn.tiny.cloud/1/mx0hg6m4ui31cy1igzvxgbkvg2ui5p76rj0b5uen9754y0xa/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>
<script>
    tinymce.init({
      selector: 'textarea',
      plugins: 'a11ychecker advcode casechange export formatpainter image editimage linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tableofcontents tinycomments tinymcespellchecker',
      toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter image editimage pageembed permanentpen table tableofcontents',
      toolbar_mode: 'floating',
      tinycomments_mode: 'embedded',
      tinycomments_author: 'Author name',
    });
  </script>
@endpush
