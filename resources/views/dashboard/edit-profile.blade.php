@extends('layouts.master')

@section('content')
<div class="card">
    <div class="card-body">
        <h5 class="card-title">Edit Profile</h5>
        <hr>
        <form action="" method="">
            <div class="mb-3">
                <label for="name" class="form-label">Nama</label>
                <input name='name' class="form-control" id="name" placeholder="Masukkan nama" required>
                {{-- <div class="invalid-feedback">
                  Please enter a message in the textarea.
                </div> --}}
            </div>
            <div class="mb-3">
                <label for="age" class="form-label">Umur</label>
                <input name='age' class="form-control" id="age" placeholder="contoh: 17">
                {{-- <div class="invalid-feedback">
                  Please enter a message in the textarea.
                </div> --}}
            </div>
            <div class="mb-3">
                <label for="address" class="form-label">Alamat</label>
                <input name='address' class="form-control" id="address" placeholder="contoh: Semarang, ID">
                {{-- <div class="invalid-feedback">
                  Please enter a message in the textarea.
                </div> --}}
            </div>
            <div class="mb-3">
                <label for="biography" class="form-label">Biografi</label>
                <textarea name='biography' class="form-control" id="biography" placeholder="Masukkan biografi"></textarea>
                {{-- <div class="invalid-feedback">
                  Please enter a message in the textarea.
                </div> --}}
            </div>
            <div class="mb-3">
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="gender" id="inlineRadio1" value="male">
                    <label class="form-check-label" for="inlineRadio1">Pria</label>
                  </div>
                  <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="gender" id="inlineRadio2" value="female">
                    <label class="form-check-label" for="inlineRadio2">Wanita</label>
                  </div>
            </div>

            <div class="mb-3">
              <label for="image" class="form-label">Upload Avatar <span style="font-size: .8rem">*(opsional)</span></label>
              <input name="image" id="image" type="file" class="form-control" aria-label="file example">
              {{-- <div class="invalid-feedback">Example invalid form file feedback</div> --}}
            </div>

            <div class="mb-3">
              <button class="btn btn-primary" type="submit"><i class='bx bx-save'></i> Simpan</button>
            </div>
          </form>
    </div>
</div>
@endsection
