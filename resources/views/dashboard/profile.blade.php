@extends('layouts.master')

@section('content')
    <div class="row g-3">
        <div class="col-12 col-md-6">
            <div class="card">
                <div class="card-body">
                    <div class="dropdown dropstart float-end">
                        <a class="text-secondary" href="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
                            <i class='bx bx-dots-vertical-rounded'></i>
                        </a>
                        <ul class="dropdown-menu w-100" aria-labelledby="dropdownMenuLink" style="min-width: 6.25rem !important;">
                        <li><a class="dropdown-item text-end" href="{{route('edit-profile')}}">Edit <i class='bx bx-message-square-edit'></i></a>
                        </li>
                        <li><a class="dropdown-item text-end" href="#">Delete <i class='bx bx-message-square-x' ></i></a>
                        </li>
                        </ul>
                    </div>
                    <h5 class="card-title">Profile</h5>
                    <hr>
                    <div class="text-center">
                        <img src="https://i.imgur.com/hczKIze.jpg" alt="" class="rounded-circle img-thumbnail mb-3" width="125px">
                        <h4>{{Auth::user()->name}}</h4>
                        <div class="text-muted small">
                            <span>20th <i class='bx bx-male-sign'></i></span>
                            <span>・</span>
                            <span><i class='bx bx-time'></i> Member Sejak 10 Maret 2022<br></span>
                            <span><i class='bx bx-map' ></i> Kabupaten Semarang, ID</span>
                        </div>
                        <p class="mt-2">Lorem ipsum dolor sit amet consectetur adipisicing elit. Tempora, aperiam.</p>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="card border-0 text-center">
                                <div class="card-body bg-light">
                                    <h6>Total<br>Pertanyaan</h6>
                                    <h5>2</h5>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="card border-0 text-center">
                                <div class="card-body bg-light">
                                    <h6>Total<br>Jawaban</h6>
                                    <h5>5</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-6">
            <div class="card">
                <div class="card-body">
                    <h5>Aktivitas</h5>
                    <hr>
                    <div class="list-group list-group-flush">
                        @for ($i = 0; $i < 5; $i++)
                            <a href="#" class="list-group-item list-group-item-action">
                                <h6>Mau bertanya tentang HTML</h6>
                                <small>Content Lorem ipsum dolor sit amet.</small>
                                <div>
                                    <span class="badge bg-info">#tag</span>
                                    <span class="badge bg-info">#tag</span>
                                    <span class="badge bg-info">#tag</span>
                                </div>
                            </a>
                        @endfor
                      </div>
                </div>
            </div>
        </div>
    </div>
@endsection
