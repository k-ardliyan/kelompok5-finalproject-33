@extends('layouts.master')

@section('content')
<div>
    <a href="/question/create" class="btn btn-outline-info mb-2""><i class='bx bx-plus' ></i> Buat Pertanyaan</a>
</div>
<div class="list-group">
    @forelse ($questions as $question)
        <a href="question/{{$question->id}}" type="button" class="card-body-border border-top mb-3 rounded-lg list-group-item list-group-item-action">
            <div class="media align-items-center p-2">
                <div class="media-body">
                    <h5 class="mb-0">
                        {{$question->title}}
                    </h5>
                    <div class="text-muted small mb-2">
                        <span class="text-muted">{{$question->user->profile->fullname}}</span>
                        <span>-</span>
                        <span class="text-muted">{{$question->created_at->format('d F Y')}}</span>
                    </div>
                    <div class="text-start mb-2">
                        {!!$question->content!!}
                    </div>
                    <div class="d-flex justify-content-between">
                        <div>
                                @foreach($question->tags as $tag)
                                    <span class="badge bg-info">{{ $tag->name }}</span>
                                @endforeach
                        </div>
                        <div>
                            <span>{{count($question->answer)}}</span>
                            Jawaban
                        </div>
                    </div>
                </div>
            </div>
        </a>
    @empty
        <h1>Belum ada pertanyaan</h1>
    @endforelse
</div>
@endsection
