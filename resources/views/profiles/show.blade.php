@extends('layouts.master')

@section('content')
    <div class="row g-3">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="dropdown dropstart float-end">
                        <a class="text-secondary" href="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
                            <i class='bx bx-dots-vertical-rounded'></i>
                        </a>
                        <ul class="dropdown-menu w-100" aria-labelledby="dropdownMenuLink" style="min-width: 6.25rem !important;">
                        <li><a class="dropdown-item text-end" href="/profile/{{$user->profile->id}}/edit">Edit <i class='bx bx-message-square-edit'></i></a>
                        </li>
                        <li><a class="dropdown-item text-end" href="/profile/{{$user->profile->id}}/delete" onclick="return confirm('Jika anda menghapus profile, sama saja anda menghapus akun. Yakin di lanjutkan?')">Delete<i class='bx bx-message-square-x' ></i></a>
                        </li>
                        </ul>
                    </div>
                    <h5 class="card-title">Profile</h5>
                    <hr>
                    <div class="text-center">
                        <img src="{{$user->profile->getAvatar()}}" alt="" class="rounded-circle img-thumbnail mb-3" width="125px">
                        <h4>{{$user->profile->fullname}}</h4>
                        <div class="text-muted small">
                            <span>{{$user->profile->age}}
                                @if (strtolower($user->profile->gender) == 'm')
                                    <i class='bx bx-male-sign'></i>
                                @elseif (strtolower($user->profile->gender) == 'f')
                                    <i class='bx bx-female-sign'></i>
                                @endif
                            </span>
                            <span>・</span>
                            <span><i class='bx bx-at'></i>{{$user->name}}<br></span>
                            <span><i class='bx bx-map' ></i> {{$user->profile->address}} </span>
                        </div>
                        <p class="mt-2"> {{$user->profile->biography}} </p>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="card border-0 text-center">
                                <div class="card-body bg-light">
                                    <h6>Total<br>Pertanyaan</h6>
                                    <h5>{{count($user->question)}}</h5>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="card border-0 text-center">
                                <div class="card-body bg-light">
                                    <h6>Total<br>Jawaban</h6>
                                    <h5>{{count($user->answer)}}</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
