@extends('layouts.master')

@section('content')
<div class="card">
    <div class="card-body">
        <h5 class="card-title">Edit Profile</h5>
        <hr>
        <form method="POST" action="/profile/{{$profile->id}}" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="mb-3">
                <label for="fullname" class="form-label">Name</label>
                <input name='fullname' class="form-control" id="fullname" placeholder="Masukkan nama" value="{{$profile->fullname}}" required>
            </div>
            <div class="mb-3">
                <label for="age" class="form-label">Age</label>
                <input name='age' class="form-control" id="age" placeholder="contoh: 17" value="{{$profile->age}}">
            </div>
            <div class="mb-3">
                <label for="address" class="form-label">Adress</label>
                <input name='address' class="form-control" id="address" placeholder="contoh: Semarang, ID" value="{{$profile->address}}">
            </div>
            <div class="mb-3">
                <label for="biography" class="form-label">About Me</label>
                <textarea name='biography' class="form-control" id="biography" placeholder="Masukkan biografi">{{$profile->biography}}</textarea>
            </div>
            <div class="mb-3">
                <label for="gender" class="form-label">Gender</label>
                <input name='gender' class="form-control" id="gender" placeholder="M / F" value="{{$profile->gender}}">
            </div>

            <div class="mb-3">
              <label for="avatar" class="form-label">Upload Avatar</label>
              <input name="avatar" id="avatar" type="file" class="form-control" aria-label="file example" value="{{$profile->avatar}}">
            </div>

            <div class="mb-3">
              <button class="btn btn-primary" type="submit"><i class='bx bx-save'></i> Simpan</button>
            </div>
          </form>
    </div>
</div>
@endsection
