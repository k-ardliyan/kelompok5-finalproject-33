<!doctype html>
<html>

<head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>Forum Diskusi - Kel 5</title>
    <link href='https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css' rel='stylesheet'>
    <link href='https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css' rel='stylesheet'>
    @stack('styles')
    <link rel="stylesheet" href="{{asset('css/dashboard.css')}}">
</head>

<body class='snippet-body'>

    <body id="body-pd">
        <header class="header" id="header">
            <div class="header_toggle"> <i class='bx bx-menu' id="header-toggle"></i> </div>
            <div class="d-inline-block">
                <a href="/profile/{{Auth::id()}}">
                    {{-- <div class="header_img ms-4 mt-2"> <img src="https://i.imgur.com/hczKIze.jpg" alt=""> </div> --}}
                    <h5 style="color: #4723D9">@<span>{{Auth::user()->name}}</span></h5>
                </a>
            </div>

        </header>

        @include('partials.sidebar')

        <!--Container Main start-->
        <div class="h-100">
            <div class="py-4">
                <div class="d-flex justify-content-between mb-3">
                    @if (Route::is('dashboard'))
                        <h4 class="align-self-center">Beranda</h4>
                    @elseif (Route::is('profile.show') || Route::is('question.show'))
                        <a href="{{route('dashboard')}}" class="btn btn-light"><i class='bx bxs-chevron-left'></i></a>
                    @elseif (Route::is('profile.edit'))
                        <a href="{{route('profile.show', Auth::id())}}" class="btn btn-light"><i class='bx bxs-chevron-left'></i></a>
                    @elseif (Route::is('question.edit'))
                        <a href="{{url()->previous()}}" class="btn btn-light"><i class='bx bxs-chevron-left'></i></a>
                    @endif
                </div>

                @yield('content')

            </div>
        </div>

        <!--Container Main end-->
        <script type='text/javascript' src='https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js'></script>
        <script type='text/javascript' src='https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js'></script>
        @stack('scripts')
        @include('sweetalert::alert')
        <script src="{{asset('js/dashboard.js')}}"></script>
    </body>
</html>
