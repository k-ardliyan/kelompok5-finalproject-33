# Final Project

## Kelompok 5

## Anggota Kelompok

- Kholifatul Ardliyan
- Ainun Nafilatur Rusyda
- Anugerah Cahaya Utama

## Tema Project

Forum Diskusi

## History Pengerjaan

Default Branch Ainun

[Lihat Repository Graph Gitlab ![new_tab](https://img.icons8.com/cotton/18/000000/external-link--v1.png)](https://gitlab.com/k-ardliyan/kelompok5-finalproject-33/-/network/ainun)

## Tech & Libary

- Laravel 8
- Bootstrap
- TinyMCE 6
- Laravel Tagging
- SweetAlert2

## Entity Relationship Diagram

![ERD](public/images/final-erd-forum-diskusi.png)

## All Link

- Demo Live Website: [Menuju Demo Website ![new_tab](https://img.icons8.com/cotton/18/000000/external-link--v1.png)](http://forum-diskusi-laravel.herokuapp.com/)
  
- Demo Video: [Lihat Demo Video ![new_tab](https://img.icons8.com/cotton/18/000000/external-link--v1.png)](https://youtu.be/XmSRYsIybBE)

## Data Login on Website

```js
email: admin@gmail.com
password: admin

email: user@gmail.com
password: user

email: guest@gmail.com
password: guest
```
