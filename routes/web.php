<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('welcome');

// Route::get('profile', function () {
//     return view('dashboard.profile');
// })->name('profile');

// Route::get('profile/edit', function () {
//     return view('dashboard.edit-profile');
// })->name('edit-profile');

Route::get('detail-question', function () {
    return view('dashboard.detail-question');
})->name('detail-question');

Route::get('create-question', function () {
    return view('dashboard.create-question');
})->name('create-question');

//Question / Pertanyaan
//Create
Route::get('/question/create', 'QuestionController@create')->name('question.create');
Route::post('/question', 'QuestionController@store');
//Read
Route::get('/question', 'QuestionController@index')->name('dashboard');
Route::get('/question/{question_id}', 'QuestionController@show')->name('question.show'); //ok
//Update
Route::get('/question/{question_id}/edit', 'QuestionController@edit')->name('question.edit');
Route::put('/question/{question_id}', 'QuestionController@update');
//Delete
Route::delete('/question/{question_id}', 'QuestionController@destroy');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/logout', 'HomeController@logout');

//Answer
//Create
Route::post('/answer', 'AnswerController@store');
//Update
Route::get('/answer/{question_id}/{answer_id}/edit', 'AnswerController@edit');
Route::put('/answer/{question_id}/{answer_id}', 'AnswerController@update');
// //Delete
Route::delete('/answer/{answer_id}/', 'AnswerController@destroy');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/logout', 'HomeController@logout');

//Profile
Route::get('/profile/create', 'ProfileController@create');
Route::post('/profile/store', 'ProfileController@store');
Route::get('/profile/{id}', 'ProfileController@show')->name('profile.show');
Route::get('/profile/{id}/edit', 'ProfileController@edit')->name('profile.edit');
Route::put('/profile/{id}', 'ProfileController@update');
Route::get('/profile/{id}/delete', 'ProfileController@destroy');
