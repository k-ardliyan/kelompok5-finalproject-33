<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table = 'profiles';

    protected $fillable = [
        'fullname',
        'avatar',
        'age',
        'address',
        'biography',
        'gender',
        'user_id',
    ];

    public function getAvatar(){
        if(!$this->avatar){
            return asset('images/default.jpg');
        }
        return asset('images/'.$this->avatar);
    }

    public function user(){
        return $this->belongsTo('App\User');
    }
}
