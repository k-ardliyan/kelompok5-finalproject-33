<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    use HasFactory;
    use \Conner\Tagging\Taggable;

    protected $table = 'questions';

    protected $fillable = [
        'title',
        'content',
        'image',
        'user_id'
    ];

    public function answer()
    {
        return $this->hasMany('App\Answer');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    // public function tags()
    // {
    //     return $this->belongsTo('App\TaggingTagged');
    // }

}
