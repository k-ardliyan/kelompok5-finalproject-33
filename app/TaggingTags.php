<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TaggingTags extends Model
{
    protected $table = 'tagging_tags';

    protected $fillable = [
        'slug',
        'name',
        'count',
    ];
}
