<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question_tag_list extends Model
{
    protected $table = 'question_tag_lists';

    protected $fillable = [
        'question_id',
        'tag_id'
    ];
}
