<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Profile;
use App\User;
use Illuminate\Support\Facades\Auth;
use Alert;

class ProfileController extends Controller
{
    public function create(){
        return view('profiles.index');
    }

    public function store(Request $request){
        $this->validate($request,[
            'fullname' => 'required',
            'biography' => 'required',
            'address' => 'required',
            'age' => 'required',
            'gender' => 'required',
            'avatar' => 'mimes:jpg,png,jpeg'
        ]);

        // dd($request->all());

        $profile = Profile::create([
            "fullname" => $request["fullname"],
            "biography" => $request["biography"],
            "address" => $request["address"],
            "age" => $request["age"],
            "gender" => $request["gender"],
            "avatar" => $request["avatar"],
            "user_id" => Auth::id()
        ]);
        if($request->hasFile('avatar')){
            $request->file('avatar')->move('images/',$request->file('avatar')->getClientOriginalName());
            $profile->avatar = $request->file('avatar')->getClientOriginalName();
            $profile->save();
        }

        Alert::success('Profile Created', 'Success');
        return redirect("/profile/". Auth::id())->with('success','Profile Successfully Created');
    }

    public function show($id){
        $user = User::find($id);
        return view('profiles.show', compact('user'));
    }

    public function edit($id){
        $profile = Profile::find($id);
        return view('profiles.edit', compact('profile'));
    }
    public function update($id, Request $request){
        $this->validate($request,[
            'fullname' => 'required',
            'biography' => 'required',
            'address' => 'required',
            'age' => 'required',
            'gender' => 'required',
            'avatar' => 'mimes:jpg,png,jpeg'
        ]);

        $profile = Profile::find($id);
        // $profile->fullname = $request->fullname;
        // $profile->biography = $request->biography;
        // $profile->address = $request->address;
        // $profile->age = $request->age;
        // $profile->gender = $request->gender;
        // $profile->avatar = $request->avatar;
        // $profile->update();
        $profile->update($request->all());
        if($request->hasFile('avatar')){
            $request->file('avatar')->move('images/',$request->file('avatar')->getClientOriginalName());
            $profile->avatar = $request->file('avatar')->getClientOriginalName();
            $profile->save();
        }

        Alert::success('Profile Updated', 'Success');
        return redirect('/profile/'.Auth::id())->with('success', 'Profile Successfully Updated');
    }

    public function destroy($id){
        $user = User::find($id);
        $user->delete();
        auth()->logout();
        Session()->flush();
        Alert::success('Profile Deleted', 'Success');
        return redirect('/');
    }
}
