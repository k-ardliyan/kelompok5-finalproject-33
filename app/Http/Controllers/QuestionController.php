<?php

namespace App\Http\Controllers;

use App\Question;
use App\Profile;
use File;
use Illuminate\Http\Request;
// use Auth;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Alert;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $questions = Question::all();

        // dd($questions);
        return view('dashboard.question', compact('questions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        return view('dashboard.create-question');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());

        $request->validate([
            'title' => 'required',
            'content' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'tags' => 'required'
        ],
        );

        $imageName = time().'.'.$request->image->extension();

        $request->image->move(public_path('images'), $imageName);


    	// $input = $request->all();

    	$tags = explode(",", $request->tags);

        // dd($tags);
    	// $question = Question::create($input);
        $questions = Question::create([
            "title" => $request["title"],
            "content" => $request["content"],
            "image" => $imageName,
            "user_id" => Auth::id()
        ]);
    	$questions->tag($tags);

        Alert::success('Success', 'Success Membuat Question');

        return redirect('/question');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $questions = Question::find($id);
        $current_user = Profile::find(Auth::user()->id);

        return view('dashboard.detail-question', compact('questions', 'current_user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $questions = Question::find($id);

        return view('dashboard.edit-question', compact('questions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required',
            'content' => 'required',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'tags' => 'required'
        ],
        );

        // check if image exists, delete, then upload new image save into public/images
        if ($request->hasFile('image')) {
            $imageName = time().'.'.$request->image->extension();

            $request->image->move(public_path('images'), $imageName);

            $questions = Question::find($id);

            File::delete(public_path('images/'.$questions->image));

            $questions->image = $imageName;
        }

    	$tags = explode(",", $request->tags);

        // dd($tags);
    	// $question = Question::create($input);
        $questions = Question::where('id', $id)->update([
            "title" => $request["title"],
            "content" => $request["content"],
            "image" => $imageName,
            "user_id" => Auth::id()
        ]);
    	// $questions->tag($tags);

        Alert::success('Success', 'Edit Question');
        return redirect('/question');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('questions')->where('id', $id)->delete();

        Alert::success('Success', 'Mengghapus Question');
        return redirect('/question');
    }
}
