<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Redirect;
use App\Answer;
use App\Question;
use Alert;

class AnswerController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());

        $request->validate([
            'answer' => 'required'
        ],
        );

        $questions = Answer::create([
            "content" => $request["answer"],
            "question_id" => $request["question_id"],
            "user_id" => Auth::id()
        ]);

        Alert::success('Success Title', 'Membuat komentar baru berhasil');
        return redirect('/question/'.$request["question_id"]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($question_id, $answer_id)
    {
        $answers = Answer::find($answer_id);
        $questions = Question::find($question_id);

        return view('dashboard.edit-answer', compact('answers', 'questions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $question_id, $answer_id)
    {
        $questions = Answer::where('id', $answer_id)->update([
            "content" => $request["answer"],
            "question_id" => $question_id,
            "user_id" => Auth::id()
        ]);

        Alert::success('Success', 'Berhasil mengubah komentar');
        return redirect('/question/'.$question_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('answers')->where('id', $id)->delete();

        Alert::success('Success', 'Komentar berhasil dihapus');
        return Redirect::back()->with('message','Komentar berhasil dihapus !');
    }
}
