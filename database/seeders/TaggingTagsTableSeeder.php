<?php

namespace Database\Seeders;

use App\TaggingTagged;
use Illuminate\Database\Seeder;
use App\TaggingTags;

class TaggingTagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tagging_tags = [
            [
                'slug' => 'tag1',
                'name' => 'tag1',
                'count' => 1,
            ],
            [
                'slug' => 'tag2',
                'name' => 'tag2',
                'count' => 1,
            ],
            [
                'slug' => 'tag3',
                'name' => 'tag3',
                'count' => 1,
            ],
            [
                'slug' => 'tag4',
                'name' => 'tag4',
                'count' => 1,
            ]
        ];

        TaggingTags::insert($tagging_tags);
    }
}
