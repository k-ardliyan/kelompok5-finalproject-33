<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Answer;

class AnswersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        for ($i = 0; $i < 15; $i++) {
            Answer::create([
                'content' => $faker->paragraph,
                'user_id' => $faker->numberBetween(1, 3),
                'question_id' => $faker->numberBetween(1, 3),
            ]);
        }
    }
}
