<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Question;

class QuestionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        $questions = [
            [
                'title' => $faker->sentence(5),
                'content' => $faker->paragraph(2),
                'image' => $faker->image('public/images', 400, 300, null, false),
                'user_id' => '1',
            ],
            [
                'title' => $faker->sentence(5),
                'content' => $faker->paragraph(2),
                'image' => $faker->image('public/images', 400, 300, null, false),
                'user_id' => '2',
            ],
            [
                'title' => $faker->sentence(5),
                'content' => $faker->paragraph(2),
                'image' => $faker->image('public/images', 400, 300, null, false),
                'user_id' => '3',
            ],
        ];

        foreach ($questions as $question) {
            Question::create($question);
        }
    }
}
