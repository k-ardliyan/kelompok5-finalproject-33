<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UsersTableSeeder::class,
            ProfilesTableSeeder::class,
            QuestionsTableSeeder::class,
            TagsTableSeeder::class,
            AnswersTableSeeder::class,
            TaggingTagsTableSeeder::class,
            TaggingTaggedTableSeeder::class,
        ]);
    }
}
