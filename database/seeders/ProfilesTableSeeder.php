<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Profile;

class ProfilesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create('id_ID');

        $profiles = [
            [
                'fullname' => 'Admin',
                'avatar' => $faker->image('public/images',480,480, 'people', false),
                'age' => $faker->numberBetween(18, 60),
                'address' => $faker->address,
                'biography' => $faker->text(200),
                'gender' => 'F',
                'user_id' => 1,
            ],
            [
                'fullname' => 'User',
                'avatar' => $faker->image('public/images',480,480, 'people', false),
                'age' => $faker->numberBetween(18, 60),
                'address' => $faker->address,
                'biography' => $faker->text(200),
                'gender' => 'M',
                'user_id' => 2,
            ],
            [
                'fullname' => 'Guest',
                'avatar' => $faker->image('public/images',480,480, 'people', false),
                'age' => $faker->numberBetween(18, 60),
                'address' => $faker->address,
                'biography' => $faker->text(200),
                'gender' => 'F',
                'user_id' => 3,
            ],
        ];

        foreach ($profiles as $profile) {
            Profile::create($profile);
        }
    }
}
