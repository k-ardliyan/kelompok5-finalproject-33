<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Tag;

class TagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        for ($i = 0; $i < 10; $i++) {
            Tag::create([
                'name' => $faker->word,
                'question_id' => $faker->numberBetween(1, 3),
            ]);
        }
    }
}
