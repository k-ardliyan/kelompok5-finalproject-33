<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\TaggingTagged;
use App\TaggingTags;

class TaggingTaggedTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tagging_tagged = [
            [
                'taggable_id' => 1,
                'taggable_type' => 'App\Question',
                'tag_name' => 'tag1',
                'tag_slug' => 'tag1',
            ],
            [
                'taggable_id' => 1,
                'taggable_type' => 'App\Question',
                'tag_name' => 'tag2',
                'tag_slug' => 'tag2',
            ],
            [
                'taggable_id' => 2,
                'taggable_type' => 'App\Question',
                'tag_name' => 'tag3',
                'tag_slug' => 'tag3',
            ],
            [
                'taggable_id' => 3,
                'taggable_type' => 'App\Question',
                'tag_name' => 'tag4',
                'tag_slug' => 'tag4',
            ],
        ];

        TaggingTagged::insert($tagging_tagged);
    }
}
